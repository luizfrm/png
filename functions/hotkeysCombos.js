import MoveMouse from "./moveMouse.js";
import KeyPress from "./keyPress.js";
import Sleep from "./sleep.js";
import robot from "robotjs";

export const revive = async () => {
  const oldMousePos = robot.getMousePos();
  MoveMouse(37, 50);
  robot.mouseClick("right");
  await Sleep(200);
  await KeyPress("q");
  robot.mouseClick("right");
  MoveMouse(oldMousePos.x, oldMousePos.y);
};

export const combo = async () => {
  KeyPress("8");
  await Sleep(700);
  KeyPress("7");
  await Sleep(700);
  KeyPress("6");
  await Sleep(700);
  KeyPress("3");
  await Sleep(100);
};
