import MouseClick from "./mouseClick.js";
import MoveMouse from "./moveMouse.js";
import KeyPress from "./keyPress.js";
import { combo, revive } from "./hotkeysCombos.js";
import Sleep from "./sleep.js";
import robot from "robotjs";
import KeyboardType from "./keyboardType.js";
import GetRandomInteger from "../libs/getRandomInteger.js";
import alert from "alert-node";
import Sound from "node-aplay";

const MINIMAP_NORTHWEST = [1719, 1];
const MINIMAP_SOUTHEAST = [1913, 156];
const precise_middle_white_minimap = [1812, 103];
const unprecise_middle_white_minimap = [1819, 82];
const MAX_SECONDS_WITHOUT_BEING_INSIDE_ICON = 5;

const reviveHotkey = "q";
const POKEMON_COORDS = { x: 0, y: 0 };

const getPokemonsInBattle = () => {
  return 2;
};

export async function huntRond(protectHotkey, iconColorId) {
  const topX = MINIMAP_NORTHWEST[0];
  const topY = MINIMAP_NORTHWEST[1];
  const bottomX = MINIMAP_SOUTHEAST[0];
  const bottomY = MINIMAP_SOUTHEAST[1];

  // look on minimap

  for (let x = topX; x <= bottomX; x += 2) {
    for (let y = topY; y <= bottomY; y += 2) {
      const color = robot.getPixelColor(x, y);
      if (color == iconColorId) {
        await MouseClick("left", x, y);
        // await MoveMouse(500, 500); // tirar o mouse do mini mapa

        // achar cruz branca que marca o personagem
        const xMiddleMiniMap = unprecise_middle_white_minimap[0];
        const yMiddleMiniMap = unprecise_middle_white_minimap[1];

        await Sleep(7000);
        KeyPress(protectHotkey);
        await Sleep(7000);

        // MouseClick("middle", 1059, 522);
        MouseClick("middle", 864, 522);

        await Sleep(5000);
        await combo();
        await revive();
        await KeyPress("e");
        await Sleep(500);
        await KeyPress("left");
        await KeyPress("e");

        await unlogIfPlayerAround();
        return true;
      }
    }
  }

  // espera padrao ate o proximo
  await Sleep(1500);
}

const possibleQuitPhrases = [
  "to de saida, flws",
  "to indo nessa ja mn, boa hunt",
  "vou sair ja vlw",
  "aqui nao ta dropando nada kkkk, vou e sair",
  "to saindo ja mn, pode upar ae",
];

const unlog = async () => {
  MoveMouse(1911, 10);
  robot.mouseClick("left");
  await Sleep(500);
  await KeyPress("enter");
};

const quitOption = async () => {
  let isStaminaOnScreen = robot.getPixelColor(1285, 998) == "fed845";
  let firstTime = true;
  while (isStaminaOnScreen) {
    await Sleep(7000);

    if (firstTime) {
      await unlog();
      firstTime = false;
    } else {
      await combo();
      await Sleep(7000);
      await unlog();
      console.log("still battle");
    }
    await revive();
  }

  isStaminaOnScreen = robot.getPixelColor(1285, 998) == "fed845";
};

const alertSound = new Sound("./alert.wav");

const advisePlayerDetect = async () => {
  const firstPlayerOnScreen = robot.getPixelColor(1770, 260) == "009100";
  const secondPlayerOnScreen = robot.getPixelColor(1770, 260 + 28) == "009100";
  if (firstPlayerOnScreen && secondPlayerOnScreen) {
    let i = 0;
    while (true) {
      alertSound.play();
      await Sleep(2000);
      i++;
    }
  }
};

const unlogIfPlayerAround = async () => {
  const firstPlayerOnScreen = robot.getPixelColor(1770, 260) == "009100";
  const secondPlayerOnScreen = robot.getPixelColor(1770, 260 + 28) == "009100";
  if (firstPlayerOnScreen && secondPlayerOnScreen) {
    await Sleep(1000);
    await KeyPress("enter");
    await Sleep(500);
    await KeyboardType(""); // possibleQuitPhrases[GetRandomInteger(0, possibleQuitPhrases.length)]
    await Sleep(1000);
    await KeyPress("enter");
    await advisePlayerDetect();
    // await quitOption();
  }
};
