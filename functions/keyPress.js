import robot from "robotjs";
import Sleep from "./sleep.js";

export default async function KeyPress(key, secondWithTogleKey) {
  if (secondWithTogleKey) {
    robot.keyToggle(key, "down");
    await Sleep(500);
    robot.keyTap(secondWithTogleKey);
    await Sleep(250);
    robot.keyToggle(key, "up");
  }
  // normal keypress
  else {
    await Sleep(10);
    robot.keyTap(key);
  }
}
