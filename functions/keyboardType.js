import KeyboardPress from "./keyPress.js";
import Sleep from "./sleep.js";

export default async function KeyboardType(text) {
  await Sleep(100);
  for (var i = 0; i < text.length; i++) {
    await KeyboardPress(text.charAt(i));
  }
}
