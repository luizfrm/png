import MoveMouse from "./moveMouse.js";
import robot from "robotjs";
import Sleep from "./sleep.js";

export default async function mouseClick(
  button, // button options: left, right, or middle
  x,
  y,
  precise = false,
  holdingKey = undefined, // holdingKey options: "shift"
  doubleClick = false
) {
  // apenas clicar com mouse em uma pos
  if (!holdingKey) {
    await MoveMouse(x, y, precise);
    robot.mouseClick(button, doubleClick);
  }
  // clicar com mouse numa pos segurando um botao do teclado
  else {
    if (holdingKey) {
      await Sleep(10);
      robot.keyToggle(holdingKey, "down");
    }
    await MoveMouse(x, y);
    robot.mouseClick(button, doubleClick);
    if (holdingKey) {
      await Sleep(10);
      robot.keyToggle(holdingKey, "up");
    }
  }
  await Sleep(50);
}
