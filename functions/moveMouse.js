/*
 * robot move mouse more human, dont move exacly the same position always
 */

import robot from "robotjs";
import GetRandomInteger from "../libs/getRandomInteger.js";
import Sleep from "./sleep.js";

export default async function moveMouse(x, y, precise = false) {
  const noSameX_ToLooksHuman = x + GetRandomInteger(-2, 2);
  const noSameY_ToLooksHuman = y + GetRandomInteger(-2, 2);
  if (precise == false) {
    robot.moveMouse(noSameX_ToLooksHuman, noSameY_ToLooksHuman);
  } else if (precise == true) {
    robot.moveMouse(x, y);
  }
  await Sleep(50);
}
