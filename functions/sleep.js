import GetRandomInteger from "../libs/getRandomInteger.js";

export default function Sleep(ms) {
  const noSameSleep_ToLooksHuman = GetRandomInteger(1, 99) + ms;
  return new Promise((resolve) => {
    setTimeout(resolve, noSameSleep_ToLooksHuman);
  });
}
