import gkm from "gkm";
import MoveMouse from "./functions/moveMouse.js";
import KeyPress from "./functions/keyPress.js";
import Sleep from "./functions/sleep.js";
import robot from "robotjs";
import { combo, revive } from "./functions/hotkeysCombos.js";
import { huntRond } from "./functions/hunting_pxg.js";

let itsBeingUsed = false;
let hunting = true;
gkm.events.on("key.*", async function (data) {
  if (itsBeingUsed == true) return;
  itsBeingUsed = true;
  if (data[0] == "Tab") {
    await revive();
  }

  if (data[0] == "Back Quote") {
    await combo();
  }

  if (data[0] == "Insert") {
  }

  itsBeingUsed = false;
});

// hunting
const PINK_LOCKER_COLOR = "ff38b6";
const PURPLE_LOCKER_COLOR = "cd57ff";
const RED_LOCKER_COLOR = "ff4000";
73;

while (hunting) {
  await huntRond("9", PINK_LOCKER_COLOR);
  await huntRond("9", PURPLE_LOCKER_COLOR);
  await huntRond("9", RED_LOCKER_COLOR);
}

// Listen to all mouse events (click, pressed, released, moved, dragged)
// gkm.events.on("mouse.*", function (data) {
//   console.log(this.event + " " + data);
// });
